<!-- header/navbar view
 -
 -	 This view is a horizontal navigation bar.
 -	 It is a part of the page header.
 -
-->

<div id="topNavbar">
	<div id="topLine"></div>
	<div class="navbar">
		<a href="<?=site_url()?>/">Spyoptic</a>
		<a href="<?=site_url()?>/shop/loadSimplePage/about-glasses">описание</a>
		<a href="<?=site_url()?>/shop/order">заказать</a>
		<a href="<?=site_url()?>/shop/loadSimplePage/delivery">доставка</a>
		<a href="<?=site_url()?>/shop/loadSimplePage/contact">контакты</a>
	</div>
</div>
