<!-- mobile navigation bar -->
<link rel="stylesheet" href="<?=CSS?>mobile/navbar.css" type="text/stylesheet" />
<div id="navbar">
	<a href="<?=site_url()?>/shop/showSunglasses">
		<img src="<?=IMG?>mobile/layout/sunglassesIcon.png" />
	</a>
	<a href="<?=site_url()?>/shop/order">
		<img src="<?=IMG?>mobile/layout/cartIcon.png" />
	</a>
	<a href="<?=site_url()?>/shop/showPeoplePhotos">
		<img src="<?=IMG?>mobile/layout/photosIcon.png" />
	</a>
	<a href="<?=site_url()?>/shop/loadSimplePage/video">
		<img src="<?=IMG?>mobile/layout/videoIcon.png" />
	</a>
	<a href="<?=site_url()?>/shop/loadSimplePage/contact">
		<img src="<?=IMG?>mobile/layout/contactIcon.png" />
	</a>
</div>
